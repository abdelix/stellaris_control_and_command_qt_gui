// Clase GUIPANEL implementa las funciones a ejecutar cuando se realizan acciones en los componentes (widgets)
// de su interfaz gráfico asociado.

#include "guipanel.h"     // Cabecera de la clase
#include "ui_guipanel.h"  // Cabecera de componentes del interfaz gráfico
#include <QtSerialPort/qserialportinfo.h> // Cabecera de funciones para comunicación USB serie
#include <QMessageBox>    // Se deben incluir cabeceras a los componentes que se vayan a crear en la clase
                          // y que no estén incluidos en el interfaz gráfico. En este caso, la ventana de PopUp
                          // que se muestra al recibir un PING de respuesta
#include <qwt.h>

extern "C" {
#include "protocol.h"    // Cabecera de funciones de gestión de tramas; se indica que está en C, ya que QTs
                         // se integra en C++, y el C puede dar problemas si no se indica.
}

 // Namespace para indicar que operaciones como "write" (que pueden tener
                            // diferentes funciones) se asocian a la funcion de QtSerialPort

#ifdef _WIN32
QT_USE_NAMESPACE_SERIALPORT
#endif

GUIPanel::GUIPanel(QWidget *parent,QSerialPort *serial_comm) :  // Inicializacion de variables
    QWidget(parent),
    ui(new Ui::GUIPanel)
    , transactionCount(0)
{
    ui->setupUi(this);                // Conecta la clase con su interfaz gráfico.
    setWindowTitle(tr("Interfaz de Control")); // Título de la ventana

    connected=false;                 // Todavía no hemos establecido la conexión USB
    // Explora los interfaces USB existentes en el sistema y los pone en el componente ComboBox
    // del interfaz gráfico
    // Nótese que todos los componentes del interfaz gráfico se acceden como "ui->nombreComponente"
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    ui->serialPortComboBox->addItem(info.portName());
    ui->serialPortComboBox->setFocus();   // Para que esta sea la ventana activa de inicio, en caso
                                          // de que haya varias

    // Las funciones CONNECT son la base del funcionamiento de QT; conectan dos componentes
    // o elementos del sistema; uno que GENERA UNA SEÑAL; y otro que EJECUTA UNA FUNCION (SLOT) al recibir dicha señal.
    // En el ejemplo se conecta la señal readyRead(), que envía el componente que controla el puerto USB serie (serial),
    // con la propia clase PanelGUI, para que ejecute su funcion readRequest() en respuesta.
    // De esa forma, en cuanto el puerto serie esté preparado para leer, se lanza una petición de datos por el
    // puerto serie.El envío de readyRead por parte de "serial" es automatico, sin necesidad de instrucciones
    // del programador

    // puerto serioe de comunicaciones que ya esta configurado y conectado
    if(serial_comm)
    {
      serial=serial_comm;
    }else
    {
        serial= new  QSerialPort(this);
    }

    connect(serial, SIGNAL(readyRead()), this, SLOT(readRequest()));

    ui->pingButton->setEnabled(false);    // Se deshabilita el botón de ping del interfaz gráfico, hasta que
                                          // se haya establecido conexión


    ui->Knob_conrol_brillo->setRange(0.0,1.0);
    ui->horizontalSlider_led_rojo->setRange(0,255);
    ui->horizontalSlider_led_verde->setRange(0,255);
    ui->horizontalSlider_led_azul->setRange(0,255);

#ifdef __ABDELFETAH__
    // Manejo de errores de qtserial
    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this,
            SLOT(handleError(QSerialPort::SerialPortError)));
#endif

    // Pestaña 1, tab 3 (Osciloscopio)
    // Configuración del objeto gráfica (a nivel gráfico)
    ui->grafica->setTitle("Osciloscopio");
    ui->grafica->setAxisTitle(QwtPlot::xBottom, "Tiempo");
    ui->grafica->setAxisTitle(QwtPlot::yLeft, "Valor");
    //ui->grafica->axisAutoScale(true); // Con Autoescala
    ui->grafica->setAxisScale(QwtPlot::yLeft, -10, 10); // Con escala definida
    ui->grafica->setAutoReplot(true); // La gráfica se actualiza si cambiamos sus datos

    // Elementos asociados a la gráfica. Se configuran sus cat¡racteristicas gráficas
    // Rejilla
    m_Grid = new QwtPlotGrid();  // Objeto tipo rejilla
    m_Grid->attach(ui->grafica); // Se enlaza la rejilla con la gráfica
    // Curva
    m_curve = new QwtPlotCurve();
    m_curve->setPen(QPen(Qt::red));
    //Fs
    ui->Knob_fs_oscope->setRange(0.005,10.0);
    // Inicialización de los valores del objeto curva
    Fs=1000;
    for (int i=0; i<N_MUESTRAS; i++) {
        xVal[i]=i*1.0/(Fs); //Multiplicarlo por el periodo de muestreo
        yVal[i]=0;
    }
    m_curve->attach(ui->grafica); // Se enlaza la curva con la gráfica

}

GUIPanel::~GUIPanel() // Destructor de la clase
{
    delete ui;   // Borra el interfaz gráfico asociado
}


// Funcion que se ocupa de procesar una TRAMA recibida a través  del puerto USB. Esta función deberá ser
// parcialmente modificada por los estudiantes para añadir nuevas respuestas a nuevos comandos de trama
void GUIPanel::readRequest()
{
    int posicion,tam;    //
    char * frame;          // Puntero a zona de memoria donde reside la trama recibida
    unsigned char command; // Para almacenar el comando de la trama entrante

    request.append(serial->readAll()); // Añade el contenido del puerto serie USB al array de bytes 'request'
    // así vamos acumulando  en el array la información que va llegando

    // Busca la posición del primer byte de fin de trama (0xFD) en el array

    posicion=request.indexOf((char)STOP_FRAME_CHAR,0);


    //Metodo poco eficiente pero seguro...

    // la condicion esta mal deberia ser posicion>=0 , antes ponia solo posicion>0
    while (posicion>=0)
    {
        frame=request.data(); // Puntero de trama al inicio del array de bytes
        tam=posicion-1;       //Caracter de inicio y fin no cuentan en el tamaño
        // Descarta posibles bytes erroneos que no se correspondan con el byte de inicio de trama
        while (((*frame)!=(char)START_FRAME_CHAR)&&(tam>0)) // Casting porque Qt va en C++ (en C no hace falta)
        {
            frame++;  // Descarta el caracter erroneo
            tam--;    // como parte de la trama recibida
        }
        // A. Disponemos de una trama encapsulada entre dos caracteres de inicio y fin, con un tamaño 'tam'
        if (tam >  0)
        {   //Paquete aparentemente correcto, se puede procesar
            frame++;  //Quito el byte de arranque (START_FRAME_CHAR, 0xFC)
            //Y proceso normalmente el paquete
            // Paso 1: Destuffing y cálculo del CRC. Si todo va bien, obtengo la trama
            // con valores actualizados y sin bytes de checksum
            tam=destuff_and_check_checksum((unsigned char *)frame,tam);
            // El tamaño se actualiza (he quitado 2bytes de CRC, mas los de "stuffing")
            if (tam>=0)
            {
                //El paquete está bien, luego procedo a tratarlo.
                command=decode_command_type(frame,0); // Obtencion del byte de Comando

                // Ventana popUP para el caso de comando PING; no te deja definirla en un "caso"
                QMessageBox ventanaca(QMessageBox::Information,tr("Evento"),tr(" RESPUESTA A PING RECIBIDA"),QMessageBox::Ok,this);
               // ventanaca.setStyleSheet("background-color: lightgrey");

                QMessageBox ventanita(QMessageBox::Critical,tr("Evento"),tr(" ERROR NACK"),QMessageBox::Ok,this);

                switch(command) // Segun el comando tengo que hacer cosas distintas
                {
                //Por ahora no se han implementado todos los comandos
                /****AQUI ES DONDE LOS ESTUDIANTES DEBEN AÑADIR NUEVAS RESPUESTAS ANTE LOS COMANDOS QUE SE ENVIEN DESDE LA TIVA **/
                case COMANDO_PING:  // Algunos comandos no tiene parametros
                    // Crea una ventana popup con el mensaje indicado
                    //statusLabel->setText(tr("  RESPUESTA A PING RECIBIDA"));
                    ventanaca.exec();
                    break;

                case COMANDO_NO_IMPLEMENTADO:
                {
                    // En otros comandos hay que extraer los parametros de la trama y copiarlos
                    // a una estructura para poder procesar su informacion
                    PARAM_COMANDO_NO_IMPLEMENTADO parametro;
                    extract_packet_command_param(frame,sizeof(parametro),&parametro);
                    // Muestra en una etiqueta (statuslabel) del GUI el mensaje
                    ui->statusLabel->setText(tr("  Comando rechazado,"));
                }
                    break;

                    //Falta por implementar la recepcion de mas tipos de comando

                case COMANDO_ESTADO_BOTONES_RESPUESTA:
                {
                    PARAM_COMANDO_ESTADO_BOTONES_RESPUESTA p;
                    extract_packet_command_param(frame,sizeof(p),&p);


                    ui->checkBox_boton_derecho->setChecked(p.botones.boton_derecho);
                    ui->checkBox_boton_izquierdo->setChecked(p.botones.boton_izquierdo);

                }
                break;

                case COMANDO_ESTADO_BOTONES_ASINCRONOS:
                {
                   PARAM_COMANDO_ESTADO_BOTONES_ASINCRONOS estado;
                   extract_packet_command_param(frame,sizeof(estado),&estado);

                   ui->checkBox_asinc_derecho->setChecked(estado.botones.boton_derecho);
                   ui->checkBox_asinc_izquierdo->setChecked(estado.botones.boton_izquierdo);
                }
                break;

                case COMANDO_TEMP_RESPUESTA :
                {
                    PARAM_COMANDO_TEMP_RESPUESTA param;
                    extract_packet_command_param(frame,sizeof(param),&param);
                    ui->thermometer->setValue(param.temp);

                }
                break;

                case COMANDO_TIME:
                {
                    PARAM_COMANDO_TIME param;
                    extract_packet_command_param(frame,sizeof(param),&param);
                    //El micro manda la hora--------------------------------------------------------------
                    ui->dateTimeEdit->setDateTime(QDateTime::fromTime_t(param.time));


                }
                break;

                case COMANDO_NACK:
                {
                    PARAM_COMANDO_NACK param;

                    extract_packet_command_param(frame,sizeof(param),&param);
                    ventanita.setText(( QString("Recibido mensaje nack : ")) + QString::number(param.error_code) );
                    ventanita.exec();

                }
                break;

                case COMANDO_OSCOPE_SAMPLES:
                {
                    PARAM_COMANDO_OSCOPE_SAMPLES param;
                    extract_packet_command_param(frame,sizeof(param),&param);
                    add_samples_to_plot(param.samples,sizeof(param));
                }
                break;

                default:
                    ui->statusLabel->setText(tr("  Recibido paquete inesperado,"));
                    break;
                }
            }
        }
        else
        {
            // B. La trama no está completa... no lo procesa, y de momento no digo nada
            ui->statusLabel->setText(tr(" Fallo trozo paquete recibido"));
        }
        request.remove(0,posicion+1); // Se elimina todo el trozo de información erroneo del array de bytes
        posicion=request.indexOf((char)STOP_FRAME_CHAR,0); // Y se busca el byte de fin de la siguiente trama
    }
}


// Funciones auxiliares y no asociadas al envio de comandos

// Establecimiento de la comunicación USB serie a través del interfaz seleccionado en la comboBox, tras pulsar el
// botón RUN del interfaz gráfico. Se establece una comunicacion a 9600bps 8N1 y sin control de flujo en el objeto
// 'serial' que es el que gestiona la comunicación USB serie en el interfaz QT
void GUIPanel::startSlave()
{

    ui->runButton->setText(QString("Desconectar"));
    serial->clearError();
    serial->blockSignals(false);

    serial->setReadBufferSize(0);


     if (!serial->isOpen()  ) {
        serial->close();
       }

#ifdef   __ABDELFETAH__

        serial->setPortName(ui->serialPortComboBox->currentText());
#else
        serial->setPort(ui->serialPortComboBox->currentText());
#endif

        if (!serial->open(QIODevice::ReadWrite)) {
            processError(tr("No puedo abrir el puerto %1, error code %2")
                         .arg(serial->portName()).arg(serial->error()));
            return;
        }

        if (!serial->setBaudRate(QSerialPort::Baud115200)) {
            processError(tr("No puedo establecer tasa de 115200 bps en el puerto %1, error code %2")
                         .arg(serial->portName()).arg(serial->error()));
            return;
        }

        if (!serial->setDataBits(QSerialPort::Data8)) {
            processError(tr("No puedo establecer 8bits de datos en el puerto %1, error code %2")
                         .arg(serial->portName()).arg(serial->error()));
            return;
        }

        if (!serial->setParity(QSerialPort::NoParity)) {
            processError(tr("NO puedo establecer parida en el puerto %1, error code %2")
                         .arg(serial->portName()).arg(serial->error()));
            return;
        }

        if (!serial->setStopBits(QSerialPort::OneStop)) {
            processError(tr("No puedo establecer 1bitStop en el puerto %1, error code %2")
                         .arg(serial->portName()).arg(serial->error()));
            return;
        }

        if (!serial->setFlowControl(QSerialPort::NoFlowControl)) {
            processError(tr("No puedo establecer el control de flujo en el puerto %1, error code %2")
                         .arg(serial->portName()).arg(serial->error()));
            return;
        }


    // Si la conexión se realiza, se deshabilita el botón RUN del interfaz gráfico
  //  ui->runButton->setEnabled(false);
    // Se indica que se ha realizado la conexión en la etiqueta 'statusLabel'
    ui->statusLabel->setText(tr("  Ejecucion, conectado al puerto %1.")
                         .arg(ui->serialPortComboBox->currentText()));
    // Y se habilita el botón PING
    ui->pingButton->setEnabled(true);
    // Variable indicadora de conexión a TRUE, para que se permita enviar comandos en respuesta
    // a eventos del interfaz gráfico
    connected=true;
}


// Funcion para la desconexion
void GUIPanel::stopSlave()
{

    ui->runButton->setText(QString("Conectar"));
    // vaciamos los buffers
    serial->clear();
    // cerramos
    serial->close();
    connected=false;
}


// Funcionauxiliar de procesamiento de errores de comunicación (usada por startSlave)
void GUIPanel::processError(const QString &s)
{
    activateRunButton(); // Activa el botón RUN
    // Muestra en la etiqueta de estado la razón del error (notese la forma de pasar argumentos a la cadena de texto)
    ui->statusLabel->setText(tr("  Detenido, %1.").arg(s));
}

void GUIPanel::activateRunButton()
{
    ui->runButton->setEnabled(true);
}


// Funciones SLOT que se crean automaticamente desde QTDesigner al activar una señal de un WIdget del interfaz gráfico
// Se suelen asociar a funciones auxiliares, en muchos caso, por comodidad.

// SLOT asociada a modificación de la comboBox
void GUIPanel::on_serialPortComboBox_currentIndexChanged(const QString &arg1)
{
    activateRunButton();  // Llama a funcion de habilitacion del boton RUN
}

// SLOT asociada a pulsación del botón RUN
void GUIPanel::on_runButton_clicked()
{
    if(!connected)
    {
        startSlave();
    }else{
        stopSlave();
    }
}

// SLOT asociada a pulsación del botón PING
void GUIPanel::on_pingButton_clicked()
{
    pingDevice();
}

// Funciones SLOT asociadas al cambio de estado de los CheckBox de los LED
void GUIPanel::on_rojo_stateChanged(int arg1)
{
    cambiaLEDs();
}

void GUIPanel::on_verde_stateChanged(int arg1)
{
    cambiaLEDs();
}

void GUIPanel::on_azul_stateChanged(int arg1)
{
    cambiaLEDs();
}

// SLOT asociada al borrado del mensaje de estado al pulsar el boton
void GUIPanel::on_statusButton_clicked()
{
    ui->statusLabel->setText(tr(""));
}

// Funciones de usuario asociadas a la ejecucion de comandos. La estructura va a ser muy parecida en casi todos los
// casos. Se va a crear una trama de un tamaño maximo (100), y se le van a introducir los elementos de
// num_secuencia, comando, y parametros.



// Envío de un comando PING
void GUIPanel::pingDevice()
{
    char paquete[MAX_FRAME_SIZE];
    int size;

     if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
     {
                 // El comando PING no necesita parametros; de ahí el NULL, y el 0 final.
                 // No vamos a usar el mecanismo de numeracion de tramas; pasamos un 0 como n de trama
                 size=create_frame((unsigned char *)paquete, COMANDO_PING, NULL, 0, MAX_FRAME_SIZE);
                 // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                 if (size>0) serial->write(paquete,size);
     }
}

void GUIPanel::on_horizontalSlider_led_rojo_sliderMoved(int position)
{
    color.setRed(position);
    cambiaLEDs();
}

void GUIPanel::on_horizontalSlider_led_verde_sliderMoved(int position)
{
    color.setGreen(position);
    cambiaLEDs();
}

void GUIPanel::on_horizontalSlider_led_azul_sliderMoved(int position)
{
    color.setBlue(position);
    cambiaLEDs();
}


// Envio de un comando LED
void GUIPanel::cambiaLEDs()
{
    PARAM_COMANDO_LEDS parametro; // Se crea una estructura con los parametros del comando
    char paquete[MAX_FRAME_SIZE]; // Array de soporte de trama
    int size;

     if (connected)
     {
            // Se rellenan los parametros del paquete (en este caso, el estado de los LED)
            parametro.red=ui->rojo->isChecked()?(color.red()):0;
            parametro.green=ui->verde->isChecked()?(color.green()):0;
            parametro.blue=ui->azul->isChecked()?(color.blue()):0;


            // Se crea la trama con n de secuencia 0; comando COMANDO_LEDS; se le pasa la
            // estructura de parametros, indicando su tamaño; el 100 final es el tamaño maximo
            // de trama
            size=create_frame((unsigned char *)paquete, COMANDO_LEDS, &parametro, sizeof(parametro), MAX_FRAME_SIZE);
            // Se se pudo crear correctamente, se envia la trama
            if (size>0) serial->write(paquete,size);
     }
}

void GUIPanel::on_Knob_conrol_brillo_valueChanged(double value)
{

    PARAM_COMANDO_BRILLO parametro; // Se crea una estructura con los parametros del comando
    char paquete[MAX_FRAME_SIZE]; // Array de soporte de trama
    int size;

     if (connected)
     {
            // Se rellenan los parametros del paquete (en este caso, el estado de los LED)
            parametro.intensity=ui->Knob_conrol_brillo->value();

            size=create_frame((unsigned char *)paquete, COMANDO_BRILLO, &parametro, sizeof(parametro), MAX_FRAME_SIZE);
            // Se se pudo crear correctamente, se envia la trama
            if (size>0) serial->write(paquete,size);
     }
}


//Cambia el color del led a traves del interfaz
void GUIPanel::on_pushButton_ledrgb_clicked()
{
    color=QColorDialog::getColor(color);
    ui->horizontalSlider_led_rojo->setValue(color.red());
    ui->horizontalSlider_led_verde->setValue(color.green());
    ui->horizontalSlider_led_azul->setValue(color.blue());
    cambiaLEDs();
}

void GUIPanel::on_pushButton_estado_botones_clicked()
{
    char paquete[MAX_FRAME_SIZE];
    int size;
    PARAM_COMANDO_ESTADO_BOTONES_PREGUNTA p;


     if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
     {
         size=create_frame((unsigned char *)paquete, COMANDO_ESTADO_BOTONES_PREGUNTA,&p, sizeof(p), MAX_FRAME_SIZE);
                 // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                 if (size>0) serial->write(paquete,size);
     }
}


void GUIPanel::on_pushButton_botones_asinc_toggled(bool checked)
{

    char paquete[MAX_FRAME_SIZE];
    int size;
    PARAM_COMANDO_BOTONES_ASINCRONOS pulsado;

    // Actualizamos en cncordancia con el boton

    pulsado.activar=checked;


     if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
     {
         size=create_frame((unsigned char *)paquete, COMANDO_BOTONES_ASINCRONOS,&pulsado, sizeof(pulsado), MAX_FRAME_SIZE);
                 // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                 if (size>0) serial->write(paquete,size);
     }

}


#ifdef __ABDELFETAH__
void GUIPanel::handleError(QSerialPort::SerialPortError error)
{
    QString erorr_str;
    if (error == QSerialPort::ResourceError) {

        erorr_str = serial->errorString();


       /* delete serial;
        serial = new QSerialPort();
       */


        connected=false;

        /*
         * Bug como una casa de grande :
         *
         */

        // si no deshabilito las señales se queda pillado enviando errores
        // cuando hay un error
       // serial->blockSignals(true);

       // QMessageBox::critical(this, tr("Critical Error"),  erorr_str);
        serial->clearError();
        serial->close();


    }


}
#endif

void GUIPanel::on_pushButton_actualizar_temp_clicked()
{
    char paquete[MAX_FRAME_SIZE];
    int size;

     if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
     {
         size=create_frame((unsigned char *)paquete, COMANDO_TEMP_PREGUNTA,NULL, 0 , MAX_FRAME_SIZE);
                 // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                 if (size>0) serial->write(paquete,size);
     }
}

void GUIPanel::actualizar_hora_micro(QDateTime t){
    char paquete[MAX_FRAME_SIZE];
    int size;
    unsigned time= t.toTime_t();

     if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
     {
         size=create_frame((unsigned char *)paquete, COMANDO_TIME,&time, sizeof(time) , MAX_FRAME_SIZE);
                 // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                 if (size>0) serial->write(paquete,size);
     }

     on_pushButton_pedir_hora_clicked();
}

void GUIPanel::on_pushButton_mandar_hora_micro_clicked()
{
    actualizar_hora_micro(QDateTime::currentDateTime());
}

void GUIPanel::on_pushButton_pedir_hora_clicked()
{
    char paquete[MAX_FRAME_SIZE];
    int size;

     if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
     {
         size=create_frame((unsigned char *)paquete, COMANDO_TIME_REQUEST,NULL, 0, MAX_FRAME_SIZE);
                 // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                 if (size>0) serial->write(paquete,size);
     }
}

void GUIPanel::on_pushButton_programar_encendido_clicked()
{
    programar_leds();
}


/*
 * Funcion que se encarga de programar el encendido temporizado de los leds
 * para ello se basa en el estado del widgets hora , fecha y checkbox_promar_leds
 */

void GUIPanel::programar_leds()
{
    char paquete[MAX_FRAME_SIZE];
    int size;
    PARAM_COMANDO_PROGRAMAR_LEDS param;
    QDateTime timeProgramed;

    actualizar_hora_micro(QDateTime::currentDateTime());

   /*
    * Inicializar timeProgramed con la fecha en la que queremos que se lleve a cabo la accion
    */
    timeProgramed.setDate(ui->calendarWidget_fecha_encender_led->selectedDate());
    timeProgramed.setTime(ui->timeEdit_hora_encender_led->time());

    param.enable=ui->checkBox_enable_leds->isChecked();
    param.time= timeProgramed.toTime_t();

    if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
    {
        size=create_frame((unsigned char *)paquete, COMANDO_PROGRAMAR_LEDS,&param, sizeof(param), MAX_FRAME_SIZE);
                // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                if (size>0) serial->write(paquete,size);
    }
}

void GUIPanel::on_pushButton_start_oscope_clicked()
{
    char paquete[MAX_FRAME_SIZE];
    int size;

    cambiar_Fs((ui->Knob_fs_oscope->value())*1000);

     if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
     {
         size=create_frame((unsigned char *)paquete, COMANDO_START_OSCOPE,NULL, 0, MAX_FRAME_SIZE);
                 // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                 if (size>0) serial->write(paquete,size);
     }
}

// Dibuja un nuevo punto de la gráfica, eliminando el último. Se llama cuando el timer de 50ms expira
void GUIPanel::add_samples_to_plot(unsigned char * samples, unsigned longitud){
    // Queremos representar los 360º (2*pi) en 100 puntos

    for (unsigned int i=0; i<N_MUESTRAS-longitud; i++) {
        yVal[i]=yVal[i+longitud];  // Desplazamos todos los datos , eliminando los mas antiguos
    }

    for(unsigned int i=0; i< longitud; i++){
        yVal[N_MUESTRAS-longitud+i]=(samples[i]*(1.0/255.0)-0.5)*10; //para que lo pinte de -5 a 5
    }

     //ui->valor->setText(tr("Val:%1").arg(yVal[99]));
     m_curve->setRawSamples(xVal,yVal,N_MUESTRAS); // Actualizamos la curva (automaticamente se repinta)
}

void GUIPanel::on_pushButton_stop_oscope_clicked()
{
    char paquete[MAX_FRAME_SIZE];
    int size;

     if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
     {
         size=create_frame((unsigned char *)paquete, COMANDO_STOP_OSCOPE,NULL, 0, MAX_FRAME_SIZE);
                 // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                 if (size>0) serial->write(paquete,size);
     }
}

void GUIPanel::cambiar_Fs(uint32_t fs){

    char paquete[MAX_FRAME_SIZE];
    int size;
    PARAM_COMANDO_CONFIG_OSCOPE param;

    for (int i=0; i<N_MUESTRAS; i++) {
        xVal[i]=i*1.0/(fs);
    }

    m_curve->setRawSamples(xVal,yVal,N_MUESTRAS); // Actualizamos la curva (automaticamente se repinta)



    param.Fs=fs;

     if (connected) // Para que no se intenten enviar datos si la conexion USB no esta activa
     {
         size=create_frame((unsigned char *)paquete, COMANDO_CONFIG_OSCOPE,&param, sizeof(param), MAX_FRAME_SIZE);
                 // Si la trama se creó correctamente, se escribe el paquete por el puerto serie USB
                 if (size>0) serial->write(paquete,size);
     }
}
