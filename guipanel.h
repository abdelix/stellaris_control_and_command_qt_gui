#ifndef GUIPANEL_H
#define GUIPANEL_H

// Cabecera de la clase de clase GUIPANEL.

#include <QWidget>
#include <QtSerialPort/qserialport.h> // Añadido de forma externa
#include <QColorDialog>
#include <QDateTime>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <stdint.h>

#define N_MUESTRAS 1000
namespace Ui {
class GUIPanel;
}

#ifdef _WIN32
QT_USE_NAMESPACE_SERIALPORT
#endif

class GUIPanel : public QWidget
{
    Q_OBJECT

    QColor color ; //color del led
    float temperatura;



public:
    //GUIPanel(QWidget *parent = 0);
    explicit GUIPanel(QWidget *parent = 0,QSerialPort *serial_comm = NULL);
    ~GUIPanel(); // Da problemas

// Estas funciones las añade automaticamente QTCreator
private slots:
    void on_pingButton_clicked();
    void on_runButton_clicked();
    void readRequest();
    void on_rojo_stateChanged(int arg1);
    void on_verde_stateChanged(int arg1);
    void on_azul_stateChanged(int arg1);
    void on_serialPortComboBox_currentIndexChanged(const QString &arg1);
    void on_statusButton_clicked();

    void on_Knob_conrol_brillo_valueChanged(double value);

    void on_pushButton_ledrgb_clicked();

    void on_pushButton_estado_botones_clicked();

#ifdef __ABDELFETAH__
    void handleError(QSerialPort::SerialPortError error);
#endif

    void on_pushButton_botones_asinc_toggled(bool checked);

    void on_pushButton_actualizar_temp_clicked();

    void on_horizontalSlider_led_rojo_sliderMoved(int position);

    void on_horizontalSlider_led_verde_sliderMoved(int position);

    void on_horizontalSlider_led_azul_sliderMoved(int position);

    void on_pushButton_mandar_hora_micro_clicked();

    void on_pushButton_pedir_hora_clicked();


    void on_pushButton_programar_encendido_clicked();

    void on_pushButton_start_oscope_clicked();

    void on_pushButton_stop_oscope_clicked();


private: // funciones privadas. Las debe añadir el programador
    void pingDevice();
    void startSlave();
    void stopSlave();
    void processError(const QString &s);
    void activateRunButton();
    void cambiaLEDs();
    void actualizar_hora_micro(QDateTime t);
    void programar_leds();
    void add_samples_to_plot(unsigned char * samples, unsigned longitud);
    void cambiar_Fs(uint32_t fs);

private:  // Variables privadas; excepto ui, las debe añadir el programador
    Ui::GUIPanel *ui;
    int transactionCount;
    bool connected;
    QSerialPort *serial;
    QByteArray request;
    QwtPlotCurve *m_curve; // Objeto que implementa la curva que dibujaremos
    QwtPlotGrid  *m_Grid;  // Objeto que implementa una rejilla en la gráfica
    double xVal[N_MUESTRAS]; // Arrays de datos a representar en la gráfica; ejeX
    double yVal[N_MUESTRAS]; // y ejeY
    unsigned int Fs;
};

#endif // GUIPANEL_H
